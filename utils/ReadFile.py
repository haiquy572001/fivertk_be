import yaml
from payload.response.ResponseCustom import ResponseCustom
def reading_error(code):
    with open("./configs/error.yaml",'r') as file :
        doc = yaml.load(file, Loader=yaml.FullLoader)
        if code in doc :
            return ResponseCustom.error_response(doc[code]['code'],doc[code]['message'])
        else :
            return ResponseCustom.error_response('INTERNAL','internal server error')
