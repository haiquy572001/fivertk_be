from fastapi import APIRouter, File,Depends, HTTPException, UploadFile
from config.method import create_upload_file, delete_files_in_folder, is_valid_youtube_link
from models.VideoClassification.recognize import VideoClassification
from payload.request.MessageRequest import MessageRequest
from payload.response.MessageResponse import MessageResponse
from payload.response.ResponseCustom import ResponseCustom
import urllib.request
import cv2
from datetime import datetime
from uploadFirebase import storage
from sqlalchemy.orm import Session
from models.Database.method import create_message
from models.Database.routes import  get_db

router = APIRouter(
    prefix="/video"
)
eval = VideoClassification()


@router.get('')
def test():
    now = datetime.now()
    print("now =", now)
    return ResponseCustom.success_response("hello world")


@router.post('')
async def generate_message(uploaded_file: UploadFile = File(...), db: Session = Depends(get_db)):
    data = await create_upload_file(uploaded_file)
    storage.child(data['url']).put(data['url'])
    download_url = storage.child(data['url']).get_url(None)
    if data != None:
        result = eval.print_results(data['url'])
        response = MessageResponse(
           type="USER", messageType='VIDEO', result=result, url = download_url)
    newResponse = create_message(db, response)    
    delete_files_in_folder('files')
    return ResponseCustom.success_response(newResponse)


@router.post('/url')
async def generate_message(data: MessageRequest):
    fileUrl = "models/VideoClassification/output/"
    urllib.request.urlretrieve(data.message, fileUrl+'video.mp4')
    newUrl = fileUrl + 'video.mp4'
    print(newUrl)

    if data != None:
        result = eval.print_results(newUrl)
        response = MessageResponse(
           type="BOT", messageType='VIDEO', result=result)
    delete_files_in_folder('models/VideoClassification/output')
    return ResponseCustom.success_response(response)
