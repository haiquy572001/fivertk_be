from fastapi import APIRouter, File, HTTPException, UploadFile
from config.method import create_upload_file
from models.TextClassification.recognize import TextClassification
from payload.request.MessageRequest import MessageRequest
from payload.response.MessageResponse import MessageResponse
from payload.response.ResponseCustom import ResponseCustom
from datetime import datetime
router = APIRouter(
    prefix="/text"
)
eval = TextClassification()


@router.get('')
def test():
    now = datetime.now()
    print("now =", now)
    return ResponseCustom.success_response("hello world")
@router.post('')
def generate_message(data: MessageRequest):
    result = eval.predict(data.message)
    print(result)
    if(result != None):
        response = MessageResponse(type="BOT", messageType='TEXT', result= True if result['label'] == 'OFFENSIVE' else False ,message= data.message)
    return ResponseCustom.success_response(response)
