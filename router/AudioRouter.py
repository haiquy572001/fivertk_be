import base64
import os
import subprocess
import time
import uuid
from fastapi import APIRouter, File,Depends, HTTPException, UploadFile
from config.method import create_upload_file, delete_files_in_folder, is_valid_youtube_link
from models.TextClassification.recognize import TextClassification
from models.VoiceRecognization.app import predictVoice
from payload.response.MessageResponse import MessageResponse
from payload.response.ResponseCustom import ResponseCustom
import urllib.request
import cv2
from datetime import datetime
import librosa
from uploadFirebase import storage
from pydub import AudioSegment
from models.Database.db import SessionLocal
from models.Database.method import create_message
from models.Database.routes import  get_db
from sqlalchemy.orm import Session

def webm_to_wav(input_file, output_file):
    # Load file
    audio = AudioSegment.from_file(input_file, format='webm')

    # Chuyển đổi sang WAV
    audio.export(output_file, format='wav')


router = APIRouter(
    prefix="/audio"
)

eval = TextClassification()


def convert_and_split(filename):
    command = ['ffmpeg', '-i', filename, '-f',
               'segment', '-segment_time', '15', 'out%09d.wav']
    subprocess.run(command, stdout=subprocess.PIPE, stdin=subprocess.PIPE)


@router.get('')
def test():
    now = datetime.now()
    print("now =", now)
    return ResponseCustom.success_response("hello world")


@router.post('')
async def generate_message(uploaded_file: UploadFile = File(...),db: Session = Depends(get_db)):
    data = await create_upload_file(uploaded_file)
    if data != None:
        dataVoice = predictVoice(data['url'])
        print('-------------')
        print(dataVoice)
        print('-------------')
        result = eval.predict(dataVoice)
        storage.child(data['url']).put(data['url'])
        download_url = storage.child(data['url']).get_url(None)
        if (result != None):
            response = MessageResponse( type="USER", messageType='AUDIO',
                                       result=True if result['label'] == 'OFFENSIVE' else False, url = download_url)
    msg = create_message(db, response)
    delete_files_in_folder('files')
    return ResponseCustom.success_response(msg)


@router.post('/record')
async def generate_message(uploaded_file: UploadFile = File(...), db: Session = Depends(get_db)):
    data = await create_upload_file(uploaded_file)
    if data != None:
        webm_to_wav(data['url'], f'files/{data["name"].split(".")[0]}.wav')
        dataVoice = predictVoice(f'files/{data["name"].split(".")[0]}.wav')
        result = eval.predict(dataVoice)
        print(dataVoice)
        storage.child(f'files/{data["name"].split(".")[0]}.wav').put(f'files/{data["name"].split(".")[0]}.wav')
        download_url = storage.child(f'files/{data["name"].split(".")[0]}.wav').get_url(None)
        if (result != None):
            response = MessageResponse( type="USER", messageType='AUDIO',
                                       result=True if result['label'] == 'OFFENSIVE' else False, url = download_url)
    msg = create_message(db, response)
    delete_files_in_folder('files')
    return ResponseCustom.success_response(msg)
