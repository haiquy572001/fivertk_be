from fastapi import APIRouter, Depends, HTTPException
from models.Chatbot.Transformer.evaluation import Evaluation
from models.Database.db import SessionLocal
from models.Database.method import create_message, get_message
from models.Database.routes import  get_db
from models.TextClassification.recognize import TextClassification
from payload.request.MessageRequest import MessageRequest
from payload.response.MessageResponse import MessageResponse
from payload.response.ResponseCustom import ResponseCustom
from sqlalchemy.orm import Session
from datetime import datetime
router = APIRouter(
    prefix="/message"
)
eval = TextClassification()
chatbotEval = Evaluation()



@router.get('')
async def get_messages(db: Session = Depends(get_db)):
    messages = get_message(db)
    return ResponseCustom.success_response(messages )


@router.post('/create')
async def create_messages(data: MessageResponse, db: Session = Depends(get_db)):
    msg = create_message(db, data)
    return ResponseCustom.success_response(msg)


@router.post('')
async def generate_message(data: MessageRequest, db: Session = Depends(get_db)):
    result = eval.predict(data.message)
    msg = chatbotEval.valueText(data.message)
    newResponse = None
    if (result != None):
        request = MessageResponse(type="USER", messageType='TEXT',
                                   result=True if result['label'] == 'OFFENSIVE' else False, message=data.message)
        create_message(db, request)
        response = MessageResponse(type="BOT", messageType='TEXT',
        result=True if result['label'] == 'OFFENSIVE' else False, message= msg['message'] if result['label'] == 'CLEAN' else None, botMsgType=msg['type'])
    if(result['label'] == 'CLEAN'):
        newResponse = create_message(db, response)
    else:
        newResponse = response
    return ResponseCustom.success_response(newResponse)
