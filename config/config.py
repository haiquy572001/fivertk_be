from pydantic import BaseSettings
# testing


class Setting(BaseSettings):
    model_url: str
    corpus_path: str

    class Config:
        env_file = '.env'
