from fastapi import File, UploadFile
import uuid
import re
import os


async def create_upload_file(uploaded_file: UploadFile = File(...)):
    # Generate a UUID
    file_uuid = str(uuid.uuid4())
    file_location = f"files/{file_uuid}-{uploaded_file.filename}"
    with open(file_location, "wb+") as file_object:
        file_object.write(uploaded_file.file.read())
    return {"name": uploaded_file.filename, "url": file_location}


def is_valid_youtube_link(link):
    pattern = r"(?:https?:\/\/)?(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/|v\/|playlist\?|watch\?.+&v=|watch\?v=|embed\/|v\/|watch\?v=)?([^#\&\?]*).*"
    match = re.match(pattern, link)
    return match is not None


def delete_files_in_folder(folder_path):
    try:
        file_list = os.listdir(folder_path)
        for file_name in file_list:
            file_path = os.path.join(folder_path, file_name)
            if os.path.isfile(file_path):
                os.remove(file_path)
        # print("All files in the folder deleted successfully.")
    except OSError as e:
        print(f"Error deleting files: {e}")
