from fastapi import FastAPI, File, Request, UploadFile
from config.method import create_upload_file
from models.Database.db import Base, engine
from router import MessageRouter, VideoRouter, TextRouter, AudioRouter
from exception.CustomException import MissingFieldException
from fastapi.responses import JSONResponse
from payload.response.ResponseCustom import ResponseCustom
from utils.ReadFile import reading_error
from functools import lru_cache
from config import config
import tldextract
from fastapi.middleware.cors import CORSMiddleware

Base.metadata.create_all(bind=engine)
app = FastAPI()


# python3 -m uvicorn main:app --reload --host 0.0.0.0 --port 80

app.include_router(MessageRouter.router)
app.include_router(VideoRouter.router)
app.include_router(TextRouter.router)
app.include_router(AudioRouter.router)

@lru_cache()
def get_settings():
    return config.Settings()


origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get('/')
def hello(request: Request):
    url = request.url._url
    sub_domain = tldextract.extract(url).subdomain
    return {'url': url,
            'sub_domain': sub_domain}

@app.post("/upload-file")
async def upload_file(uploaded_file: UploadFile = File(...)):
    create_upload_file(uploaded_file)


@app.exception_handler(MissingFieldException)
async def missing_field_exception(request: Request, ex: MissingFieldException):

    return JSONResponse(
        status_code=ex.status_code,
        content=reading_error(ex.message)
    )
