from pydantic import BaseModel
from contants import MessageContants


class ResponseCustom:
    @staticmethod
    def success_response(data=None, meta=None):
        response = {
            "status": MessageContants.SUCCESS_MSG
        }
        if data is not None:
            response["data"] = data
        if meta is not None:
            response["meta"] = meta
        return response

    @staticmethod
    def error_response(code, message):
        response = {
            "status": MessageContants.UNSUCCESS_MSG,
            "error": {
                "code": code,
                "message": message
            }
        }
        return response
