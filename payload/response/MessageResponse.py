from typing import Optional
from pydantic import BaseModel
class MessageResponse(BaseModel):
    message : Optional[str]
    messageType: str
    result: Optional[bool]
    type: str
    botMsgType: Optional[str]
    url: Optional[str]

