from pydantic import BaseModel, validator, root_validator
from exception.CustomException import MissingFieldException
from contants.MessageContants import MSG_CAN_NOT_BE_BLANK


class MessageRequest(BaseModel):
    message: str

    @root_validator(pre=True)
    def check_missing_field(cls, values):
        if 'message' not in values or values['message'] == '':
            raise MissingFieldException(MSG_CAN_NOT_BE_BLANK)
        return values

    @validator('message')
    def validate_message(cls, v):
        if v.isspace():
            raise MissingFieldException(MSG_CAN_NOT_BE_BLANK)
        return v
    
