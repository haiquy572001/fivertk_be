import tensorflow as tf

from models.Chatbot.Transformer.common.DataLoader import DataLoader
from models.Chatbot.Transformer.common.MultiHeadAttention import MultiHeadAttention
from models.Chatbot.Transformer.common.PositionalEncoding import PositionalEncoding
from models.Chatbot.Transformer.layer.transformer import create_look_ahead_mask, create_padding_mask


class Evaluation:
    def __init__(self):
        self.model = self.load_model(
            'models/Chatbot/Transformer/model_chatbot.h5')
        self.dataLoader = DataLoader()

    def load_model(self, fileName):
        model = tf.keras.models.load_model(
            fileName,
            custom_objects={
                "PositionalEncoding": PositionalEncoding,
                "MultiHeadAttention": MultiHeadAttention,
                "create_look_ahead_mask": create_look_ahead_mask,
                "create_padding_mask": create_padding_mask,
            },
            compile=False,
        )

        return model

    def evaluate(self,sentence, model, dataLoader):
        # sentence = dataLoader.preprocess_sentence(sentence)

        sentence = tf.expand_dims(
            dataLoader.START_TOKEN + dataLoader.tokenizer.encode(sentence) + dataLoader.END_TOKEN, axis=0
        )

        output = tf.expand_dims(dataLoader.START_TOKEN, 0)

        for i in range(dataLoader.MAX_LENGTH):
            predictions = model(inputs=[sentence, output], training=False)

            # select the last word from the seq_len dimension
            predictions = predictions[:, -1:, :]
            predicted_id = tf.cast(tf.argmax(predictions, axis=-1), tf.int32)

            # return the result if the predicted_id is equal to the end token
            if tf.equal(predicted_id, dataLoader.END_TOKEN[0]):
                break

            # concatenated the predicted_id to the output which is given to the decoder
            # as its input.
            output = tf.concat([output, predicted_id], axis=-1)

        return tf.squeeze(output, axis=0)

    def predict(self, sentence, model, dataLoader):
        prediction = self.evaluate(sentence, model, dataLoader)
        predicted_sentence = dataLoader.tokenizer.decode(
            [i for i in prediction if i < dataLoader.tokenizer.vocab_size]
        )
        return predicted_sentence

    def valueText(self, question):
       return {"type": "SUCCESS", "message": self.predict(question, self.model, self.dataLoader)}
