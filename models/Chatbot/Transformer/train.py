import tensorflow as tf
import matplotlib.pyplot as plt
from common.CustomSchedule import CustomSchedule
from common.Loss_Function import loss_function
from common.accuracy import accuracy
from models.Chatbot.Transformer.common.DataLoader import DataLoader
from models.Chatbot.Transformer.layer.transformer import transformer

# ---------- init the hyperparameter ----------
try:
    tpu = tf.distribute.cluster_resolver.TPUClusterResolver()
    print("Running on TPU {}".format(tpu.cluster_spec().as_dict()["worker"]))
except ValueError:
    tpu = None

if tpu:
    tf.config.experimental_connect_to_cluster(tpu)
    tf.tpu.experimental.initialize_tpu_system(tpu)
    strategy = tf.distribute.experimental.TPUStrategy(tpu)
else:
    strategy = tf.distribute.get_strategy()

print(f"REPLICAS: {strategy.num_replicas_in_sync}")


def main():
    # Maximum sentence length
    MAX_LENGTH = 15

    # For Transformer
    NUM_LAYERS = 2
    D_MODEL = 256
    NUM_HEADS = 8
    UNITS = 512
    DROPOUT = 0.1

    EPOCHS = 50
    # ckear backend
    tf.keras.backend.clear_session()

    dataLoader = DataLoader(MAX_LENGTH=MAX_LENGTH)

    learning_rate = CustomSchedule(D_MODEL)

    optimizer = tf.keras.optimizers.Adam(
        learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-7
    )

    # optimizer = tf.keras.optimizers.Adam(
    #     learning_rate,
    #     beta_1=0.9,
    #     beta_2=0.98,
    #     epsilon=1e-9,
    # )

    with strategy.scope():
        model = transformer(
            vocab_size=dataLoader.VOCAB_SIZE,
            num_layers=NUM_LAYERS,
            units=UNITS,
            d_model=D_MODEL,
            num_heads=NUM_HEADS,
            dropout=DROPOUT,
        )
        model.compile(optimizer=optimizer,
                      loss=loss_function, metrics=[accuracy])

    # show the detals of model
    model.summary()

    # Train the model
    history = model.fit(dataLoader.dataset, epochs=EPOCHS)

    # Save the image
    plt.plot(history.history['loss'])
    plt.title('Training loss')
    plt.xlabel('epoch')
    loss_image_path = 'loss_curve.png'
    plt.savefig(loss_image_path)
    plt.close()

    # train the model
    # model.fit(dataLoader.dataset, epochs=EPOCHS)
    # save the model
    fileName = "model_chatbot.h5"
    tf.keras.models.save_model(
        model, filepath=fileName, include_optimizer=False)

    del model
    tf.keras.backend.clear_session


if __name__ == "__main__":
    main()
