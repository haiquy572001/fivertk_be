import os
import tensorflow as tf
import re
import tensorflow_datasets as tfds


class DataLoader():
    def __init__(
        self,
        MAX_SAMPLES=50000,
        MAX_LENGTH=15,
        BUFFER_SIZE=20000
    ):
        # check hardware accelerator
        try:
            tpu = tf.distribute.cluster_resolver.TPUClusterResolver()
            print("Running on TPU {}".format(
                tpu.cluster_spec().as_dict()["worker"]))
        except ValueError:
            tpu = None

        if tpu:
            tf.config.experimental_connect_to_cluster(tpu)
            tf.tpu.experimental.initialize_tpu_system(tpu)
            strategy = tf.distribute.experimental.TPUStrategy(tpu)
        else:
            strategy = tf.distribute.get_strategy()

        # Maximum sentence length
        self.MAX_LENGTH = MAX_LENGTH
        # Maximum number of samples to preprocess
        self.MAX_SAMPLES = MAX_SAMPLES
        # For tf.data.Dataset
        self.BATCH_SIZE = 64 * strategy.num_replicas_in_sync
        self.BUFFER_SIZE = BUFFER_SIZE

        self.MAX_SAMPLES = MAX_SAMPLES
        self.meta_questions, self.meta_answers = self.load_conversations()
        self.tokenizer, self.START_TOKEN, self.END_TOKEN, self.VOCAB_SIZE = self.set_parameters()
        self.questions, self.answers = self.tokenize_and_filter()
        self.dataset = self.dataset_generator()

    def load_conversations(self):
        corpus_file = "models/Chatbot/data/test_output.txt"
        inputs, outputs = [], []
        with open(corpus_file) as f:
            lines = f.readlines()
        for i in range(0, len(lines) - 1, 2):
            inputs.append(lines[i])
            outputs.append(lines[i+1])
        return inputs, outputs

    def set_parameters(self):
        # Build tokenizer using tfds for both questions and answers
        tokenizer = tfds.deprecated.text.SubwordTextEncoder.build_from_corpus(
            self.meta_questions + self.meta_answers, target_vocab_size=2**13
        )

        # Define start and end token to indicate the start and end of a sentence
        START_TOKEN, END_TOKEN = [tokenizer.vocab_size], [
            tokenizer.vocab_size + 1]

        # Vocabulary size plus start and end token
        VOCAB_SIZE = tokenizer.vocab_size + 2

        return tokenizer, START_TOKEN, END_TOKEN, VOCAB_SIZE

    # Tokenize, filter and pad sentences

    def tokenize_and_filter(self):
        tokenized_inputs, tokenized_outputs = [], []

        for (sentence1, sentence2) in zip(self.meta_questions, self.meta_answers):
            # tokenize sentence
            sentence1 = self.START_TOKEN + \
                self.tokenizer.encode(sentence1) + self.END_TOKEN
            sentence2 = self.START_TOKEN + \
                self.tokenizer.encode(sentence2) + self.END_TOKEN
            # check tokenized sentence max length
            if len(sentence1) <= self.MAX_LENGTH and len(sentence2) <= self.MAX_LENGTH:
                tokenized_inputs.append(sentence1)
                tokenized_outputs.append(sentence2)

        # pad tokenized sentences
        tokenized_inputs = tf.keras.preprocessing.sequence.pad_sequences(
            tokenized_inputs, maxlen=self.MAX_LENGTH, padding="post"
        )
        tokenized_outputs = tf.keras.preprocessing.sequence.pad_sequences(
            tokenized_outputs, maxlen=self.MAX_LENGTH, padding="post"
        )

        return tokenized_inputs, tokenized_outputs

    def dataset_generator(self):
        # decoder inputs use the previous target as input
        # remove START_TOKEN from targets
        dataset = tf.data.Dataset.from_tensor_slices(
            (
                {"inputs": self.questions, "dec_inputs": self.answers[:, :-1]},
                {"outputs": self.answers[:, 1:]},
            )
        )

        dataset = dataset.cache()
        dataset = dataset.shuffle(self.BUFFER_SIZE)
        dataset = dataset.batch(self.BATCH_SIZE)
        dataset = dataset.prefetch(tf.data.AUTOTUNE)

        return dataset
