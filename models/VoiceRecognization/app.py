import librosa
from models.VoiceRecognization.infer import SpeechRecognization
import sys
sys.path.append('models/VoiceRecognization')

config = 'models/VoiceRecognization/configs/quartznet12x1_vi.yaml'
encoder_checkpoint = 'models/VoiceRecognization/models/acoustic_model/JasperEncoder-STEP-289936.pt'
decoder_checkpoint = 'models/VoiceRecognization/models/acoustic_model/JasperDecoderForCTC-STEP-289936.pt'
lm_path = 'models/VoiceRecognization/models/language_model/5-gram-lm.binary'

asrSpeech = SpeechRecognization(
    config_file=config,
    encoder_checkpoint=encoder_checkpoint,
    decoder_checkpoint=decoder_checkpoint,
    lm_path=lm_path,
    beam_width=50
)


def predictVoice(filepath):
    audio_signal, _ = librosa.load(filepath, sr=16000)
    transcript = asrSpeech.transcribe(audio_signal)
    return transcript
