import os
import pickle
from keras.models import load_model
from keras.utils import pad_sequences
import tensorflow as tf
class TextClassification:
    def __init__(self):
        self.sequence_length = 20
        self.embedding_dim = 300
        self.batch_size = 256
        self.epochs = 40
        self.drop = 0.5
        self.model = load_model('models/TextClassification/model_text.h5')
    def decode_text(self,score):
        label = "CLEAN"
        if score == 1:
            label = "OFFENSIVE"

        return label
    
    def predict(self,text):
        # loading
        with open('models/TextClassification/token.pickle', 'rb') as handle:
            tokenizer = pickle.load(handle)
        # Tokenize text
        x_test = pad_sequences(tokenizer.texts_to_sequences([text.lower()]), maxlen=self.sequence_length)
        # Predict
        score = self.model.predict([x_test], batch_size=self.batch_size, verbose=0)
        # Decode sentiment
        label = self.decode_text(score.argmax(axis=-1)[0])

        return {"label": label, "score": score}  