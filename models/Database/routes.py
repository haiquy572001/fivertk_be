from fastapi import APIRouter, HTTPException, Path
from fastapi import Depends
from sqlalchemy.orm import Session
from models.Database.db import SessionLocal
from models.Database.message import Message
from models.Database.method import create_message, get_message

from payload.response.MessageResponse import MessageResponse
from payload.response.ResponseCustom import ResponseCustom

router = APIRouter()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# async def createMessage(request: MessageResponse, db: Session = Depends(get_db)):
#    print(request)
#    await create_message(db, message=request)


# async def getMessages(skip: int = 0, limit: int = 20, db: Session = Depends(get_db)):
#     _message = db.query(Message).offset(skip).limit(limit).all()
#     return ResponseCustom.success_response(_message)

