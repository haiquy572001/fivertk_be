from typing import List, Optional, Generic, TypeVar
from pydantic import BaseModel , Field
from pydantic.generics import GenericModel

T = TypeVar('T')


class MessageSchema(BaseModel):
    id: Optional[int] = None
    message: Optional[str] = None
    messageType: Optional[str] = None
    result: Optional[bool] = None
    type: Optional[str] = None
    botMsgType: Optional[str] = None
    url: Optional[bool] = None

    class Config:
        orm_mode = True
