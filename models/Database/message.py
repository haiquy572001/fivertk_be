from sqlalchemy import  Column, Integer, String, Boolean

from models.Database.db import Base

class Message(Base):
    __tablename__ ="message"

    id = Column(Integer, primary_key=True, index=True)
    message = Column(String, nullable=True)
    messageType = Column(String)
    result = Column(Boolean, nullable=True)
    type = Column(String)
    botMsgType = Column(String ,nullable=True)
    url = Column(String, nullable=True)