from sqlalchemy.orm import Session

from models.Database.message import Message
from models.Database.schema import MessageSchema



def get_message(db: Session, skip: int = 0, limit: int = 1000):
    return db.query(Message).offset(skip).limit(limit).all()


def create_message(db: Session, message: MessageSchema):
    _message = Message(message=message.message, messageType=message.messageType, result = message.result, type = message.type, botMsgType=message.botMsgType, url = message.url)
    db.add(_message)
    db.commit()
    db.refresh(_message)
    return _message
