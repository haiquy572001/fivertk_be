from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DATABASE_URL = 'postgresql://postgres:freestay@103.137.185.31:5432/chatbot'

# DATABASE_URL = 'postgresql://postgres:lhqlhdlhn@localhost:5432/chatbot'

engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush = False, bind=engine)
Base = declarative_base()